int vel = 70; //velocidad asignada a los motores


int si = 3; // pin del sensor derecho
int sd = 2; // pin del sensor izquierdo 

// pines de señal para los motores 

  // motor izquierdo
int in1 = 9; //pin asignado a tierra del motor izquierdo
int in2 = 10;  //pin asignado a corriente del motor izquierdo

  // motor derecho
int in3 = 5; //pin asignado a tierra del motor derecho
int in4 = 6; //pin asignado a corriente del motor derecho

// variable de lectura para valores de sensores  
int ValorIzquierdo;
int ValorDerecho;

//Declaración de variables de entrada y de salida
void setup() {
  pinMode(si, INPUT);
  pinMode(sd, INPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  Serial.begin(9600); // comunicacion serial para poder leer los valores en la pc  
}

void sensorReading(){
  ValorIzquierdo = digitalRead(si); // lectura del sensor izquierdo  
  ValorDerecho = digitalRead(sd); // lectura del sensor derecho 
  delay(10);


  Serial.print(ValorIzquierdo);
  Serial.print(ValorDerecho);

}

void Adelante(){
  analogWrite(in1, LOW); 
  analogWrite(in2, vel);  
  analogWrite(in3, LOW);
  analogWrite(in4, vel);
}

void GiroDerecha(){
  // Motor izquierdo
  analogWrite(in1, LOW);
  analogWrite(in2, vel);
  // Motor derecho
  analogWrite(in3, LOW);
  analogWrite(in4, LOW); 
}
void GiroIzquierda(){
  //Motor izquierdo
  analogWrite(in1, LOW);
  analogWrite(in2, LOW);
  // Motor derecho
  analogWrite(in3, LOW);
  analogWrite(in4, vel); 
}


void ParoTotal(){
   //Motor izquierdo
  analogWrite(in1, LOW);
  analogWrite(in2, LOW);
  // Motor derecho
  analogWrite(in3, LOW);
  analogWrite(in4, LOW);
}

// Deteccion de valores de los sensores
void loop() {
  
 sensorReading();
 
  if(ValorIzquierdo == 0 && ValorDerecho == 1){
    GiroIzquierda();
  }
  
  else if(ValorIzquierdo == 1 && ValorDerecho == 0 ){
    GiroDerecha();
  }
  
  else if(ValorIzquierdo == 1 && ValorDerecho == 1){
    Adelante();
  }
  
  else if(ValorIzquierdo == 0 && ValorDerecho == 0){
    ParoTotal();
  }

}
 
